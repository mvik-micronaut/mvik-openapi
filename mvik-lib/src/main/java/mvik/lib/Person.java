package mvik.lib;

import java.time.Instant;

public class Person {

    private String name;
    private Instant bornAt;

	public Person() {
	}

	public Person(String name, Instant bornAt) {
		this.name = name;
		this.bornAt = bornAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Instant getBornAt() {
		return bornAt;
	}

	public void setBornAt(Instant bornAt) {
		this.bornAt = bornAt;
	}
}