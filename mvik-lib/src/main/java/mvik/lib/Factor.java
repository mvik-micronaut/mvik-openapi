package mvik.lib;

public enum Factor {
	Small, 
	Medium, 
	Large,
	Gigantic;
}