package mvik.openapi.microboot;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(info = @Info(title = "mvik-openapi-microboot", version = "0.1"))
public class MicronautApplication {

    public static void main(String[] args) {
        Micronaut.run(MicronautApplication.class);
    }

}
