package mvik.openapi.microboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroBootApplication.class, args);
	}

}
