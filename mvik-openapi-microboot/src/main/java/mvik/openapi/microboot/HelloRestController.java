package mvik.openapi.microboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * Full path on methods works correctly
 */
@RestController
@RequestMapping
public class HelloRestController {

    @GetMapping("/api/hellos/{name}")
    Map<String, String> hello(@PathVariable String name) {
        return Collections.singletonMap("hello", name);
    }
}
