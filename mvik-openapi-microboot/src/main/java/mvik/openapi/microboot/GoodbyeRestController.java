package mvik.openapi.microboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * Starting path on rest controller, specifics on the method, does not work as expected.
 */
@RestController
@RequestMapping("/api/goodbyes")
public class GoodbyeRestController {

    @GetMapping("/{name}")
    Map<String, String> goodbye(@PathVariable String name) {
        return Collections.singletonMap("goodbye", name);
    }

}
