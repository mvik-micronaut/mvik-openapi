package mvik.openapi.groovy

enum Size {
    Tiny,
    Medium,
    Big
}