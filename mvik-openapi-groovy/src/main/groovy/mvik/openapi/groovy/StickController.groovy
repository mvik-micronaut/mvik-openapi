package mvik.openapi.groovy

import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue

import javax.annotation.Nullable

@CompileStatic
@Controller('/api/sticks')
class StickController {

    @Get
    List<Stick> sticks(@QueryValue @Nullable Length length) {
        return []
    }

    @Get('/{type}')
    Stick findStick(@PathVariable String type) {
        return new Stick(type: type)
    }

    @Post
    Stick createStick(@Body Stick stick) {
        return stick
    }
}
