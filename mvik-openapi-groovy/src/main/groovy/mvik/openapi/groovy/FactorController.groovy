package mvik.openapi.groovy

import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import mvik.lib.Factor

@CompileStatic
@Controller('/api/factors')
class FactorController {

    @Get
    List<Factor> factors() {
        return []
    }

    @Get('/{factor}')
    Factor findFactor(String factor) {
        return Factor.valueOf(factor)
    }
}
