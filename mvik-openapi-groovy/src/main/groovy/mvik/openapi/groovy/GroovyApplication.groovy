package mvik.openapi.groovy

import groovy.transform.CompileStatic
import io.micronaut.runtime.Micronaut
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(title = "mvik-openapi-groovy", version = "0.1")
)
@CompileStatic
class GroovyApplication {
    static void main(String[] args) {
        Micronaut.run(GroovyApplication)
    }
}