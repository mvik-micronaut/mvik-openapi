package mvik.openapi.groovy

import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import mvik.lib.Person

@CompileStatic
@Controller('/api/persons')
class PersonController {

    @Get
    List<Person> persons() {
        return []
    }

    @Get('/{name}')
    Person findPerson(@PathVariable String name) {
        return new Person(name: name)
    }

    @Post
    Person createPerson(@Body Person person) {
        return person
    }
}
