package mvik.openapi.groovy

import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue

import javax.annotation.Nullable

@CompileStatic
@Controller('/api/animals')
class AnimalController {

    @Get
    List<Animal> animals(@QueryValue @Nullable Size size) {
        return []
    }

    @Get('/{name}')
    Animal findAnimal(@PathVariable String name) {
        return new Animal(name: name)
    }

    @Post
    Animal createAnimal(@Body Animal animal) {
        return animal
    }
}
