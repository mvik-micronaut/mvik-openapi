package mvik.openapi.groovy;

public class Stick {

    private String type;
    private Length length;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Length getLength() {
        return length;
    }

    public void setLength(Length length) {
        this.length = length;
    }
}
