package mvik.openapi.groovy;

public enum Length {
    Short,
    Long
}
