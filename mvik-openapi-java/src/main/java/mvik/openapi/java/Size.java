package mvik.openapi.java;

public enum Size {
    Tiny,
    Medium,
    Big
}
