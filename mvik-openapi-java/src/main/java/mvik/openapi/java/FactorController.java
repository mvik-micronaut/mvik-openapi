package mvik.openapi.java;

import java.util.Collections;
import java.util.List;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import mvik.lib.Factor;

@Controller("/factors")
public class FactorController {

    @Get
    List<Factor> factors() {
        return Collections.emptyList();
    }

    @Get("/{factor}")
    Factor findFactor(String factor) {
        return Factor.valueOf(factor);
    }
}