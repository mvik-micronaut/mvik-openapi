package mvik.openapi.java;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(title = "mvik-openapi-java", version = "0.1")
)
public class JavaApplication {

    public static void main(String[] args) {
        Micronaut.run(JavaApplication.class);
    }
}