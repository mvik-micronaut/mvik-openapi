package mvik.openapi.java;

import java.util.Collections;
import java.util.List;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import mvik.lib.Person;

@Controller("/persons")
public class PersonController {

    @Get
    List<Person> persons() {
        return Collections.emptyList();
    }

    @Get("/{name}")
    Person findPerson(@PathVariable String name) {
        return new Person(name, null);
    }

}