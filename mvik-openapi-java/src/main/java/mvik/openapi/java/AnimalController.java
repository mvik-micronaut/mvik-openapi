package mvik.openapi.java;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;

@Controller("/api/animals")
class AnimalController {

    @Get
    List<Animal> animals(@QueryValue @Nullable Size size) {
        return Collections.emptyList();
    }

    @Get("/{name}")
    Animal findAnimal(@PathVariable String name) {
        return new Animal(name);
    }

    @Post
    Animal createAnimal(@Body Animal animal) {
        return animal;
    }
}
